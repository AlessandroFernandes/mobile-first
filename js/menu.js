document
	.querySelector('.abrir-menu')
	.addEventListener('click', () =>
		document.documentElement.classList.add('menu-ativo'),
	);

document
	.querySelector('.fechar-menu')
	.addEventListener('click', () =>
		document.documentElement.classList.remove('menu-ativo'),
	);

document.documentElement.onclick = event => {
	if (event.target === document.documentElement) {
		document.documentElement.classList.remove('menu-ativo');
	}
};
